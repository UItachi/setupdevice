## My Raspberry Pi Configuration



#### Overview

* [OS Setup](#OS-Setup)
* [Package Manager](#Package-Manager)
* [Command Line Tool](#Command-Line-Tool)
* [Service](#Service)
* [Application](#Application)
* [Command (System)](#Command-(System))
* [Configuration](#Configuration)
* [tmux configuration](#tmux-configuration)
* [OpenWrt](#OpenWrt)
* [References](#References)





#### OS Setup

* Download [SD Memory Card Formatter](https://www.sdcard.org/downloads/formatter_4/index.html) to format the SD card.
* Download OS Image.
  * Official [Raspbian OS Image](https://www.raspberrypi.org/downloads/)
  * Download [PiBakery](https://www.pibakery.org/docs/install-mac.html).
* Writing an image to the SD card.
  * Use [etcher](https://www.balena.io/etcher) to burn the image to SD driver on macOS.
  * Use `dd` command on linux and macOS.
    1. Check the physical driver and logical driver name, e.g. `disk0`, `disk1` in macOS.
       * `diskutil list`
       * Launch `Disk Utility`, select the physical volume, check `Info`.
       * `Apple Menu` -> `About This Mac` -> `System Report...` -> `Hardware` -> `USB` or `Storage`
    2. `diskutil unmountDisk /dev/diskX`
    3. `sudo dd if=/path/to/file.img of=/dev/diskX bs=2M conv=fsync`
  * Use [Win32 Disk Imager](https://sourceforge.net/projects/win32diskimager/) on Windows.
* Check kinds of logs under the `/var/log` directory.
  * `/var/log/boot.log`
  * `/var/log/syslog`
  * `/var/log/messages`





#### Package Manager

| apt              | replacement          | notes                                               |
| ---------------- | -------------------- | --------------------------------------------------- |
| apt install      | apt-get install      | install package                                     |
| apt remove       | apt-get remove       | remove package                                      |
| apt purge        | apt-get purge        | remove package and configuration files              |
| apt update       | apt-get update       | reload repository index                             |
| apt upgrade      | apt-get upgrade      | upgrade all the outdated packages                   |
| apt autoremove   | apt-get autoremove   | remove unused packages automatically                |
| apt full-upgrade | apt-get dist-upgrade | upgrade packages with auto-handling of dependencies |
| apt search       | apt-cache search     | search package                                      |
| apt show         | apt-cache show       | show details                                        |
| apt policy       | apt-cache policy     | <not-yet>                                           |
| apt list         | dpkg list            | list the local installed packages                   |
| apt edit-sources | <none>               | edit the sources.list                               |



#### Command

* `[sudo] raspi-config`, launch the core system configuration interface.
* `startx`, switch to the desktop GUI interface from command line.



#### Command Line Tool

- Linuxbrew, the Homebrew package manager for Linux. http://linuxbrew.sh/
- NetworkManager (nmcli)
- tmux





#### Service

- avahi-daemon, [https://linux.die.net/man/8/avahi-daemon](https://linux.die.net/man/8/avahi-daemon)



> To check all the service status: sudo service —status-all
>
> To check the specified service status: sudo service service_name status.*



#### Application

- deluge, a fully-featured cross-platform BitTorrent client. [http://dev.deluge-torrent.org/](http://dev.deluge-torrent.org/)
- omxplayer, a commandline OMX player for the Raspberry Pi. https://github.com/popcornmix/omxplayer,
- neofetch, a command line tool to check system information. https://github.com/dylanaraps/neofetch



#### Command (System)

- `dmesg`, print or control the kernel ring buffer, it's useful to diagnose the problems on os kernel related.
- `raspi-config`, a wonderful command tool for raspberry, need sudo.
- `alsamixer`, adjust the volume of a raspberry pi.
- `hostname`, check the hostname (without domain)
- `hostname -I`, check the local ip address
- `nmap -sn 192.168.1.0/24`, ping scan on the whole subnet range. (used in ssh client.)





#### Configuration

* Change the DNS server.
  1. Append `static domain_name_servers=8.8.4.4 8.8.8.8` in the end of file `/etc/dhcpcd.conf`.
  2. Restart the `dhcpd` service: `sudo systemctl restart dhcpcd.service`.
* Change the [source.list](https://manpages.debian.org/jessie/apt/sources.list.5.en.html) for `apt-get`.
  * [RaspbianRepository](https://www.raspbian.org/RaspbianRepository)
  * [tsinghua source](https://mirror.tuna.tsinghua.edu.cn/help/raspbian)
  * `cat /etc/apt/sources.list.d/ustc.list`: `deb http://mirrors.ustc.edu.cn/raspbian/raspbian/ stretch main contrib non-free rpi`
  * `cat /etc/apt/sources.list.d/raspi.list`: `deb http://mirrors.ustc.edu.cn/archive.raspberrypi.org/debian/ stretch main ui`
* `sudo apt-get clean && sudo apt-get update && sudo apt-get upgrade`



#### GFW Proxy

1. ~~Install  `shadowsocks` client via `sudo apt-get install shadowsocks`~~ Check the [v2ray](./v2ray-vps.md) item instead.

   * ~~Edit the `/etc/shadowsocks/config.json`, fill the `server`, `server_port`, `password`,  `method` by shadowsocks server configuration.~~
   * ~~Make the shadowsocks service client start on system startup.~~
     * ~~Add the line `/usr/bin/sslocal -c /etc/shadowsocks/config.json -d start` before the `exit 0`.~~
     * ~~Check the service status by `sudo systemctl status rc-local.service`, the status result should show `active (running)` for the `sslocal` task. Run command `sudo reboot`  if necessary.~~

2. Forward the http/https traffic to socks5(shadowsocks) via [polipo](https://github.com/jech/polipo)

   * Install the software via `sudo apt-get install polipo`.

   * Configure the forward rule in file `/etc/polipo/config`.

     * ```properties
       # This file only needs to list configuration variables that deviate
       # from the default values.  See /usr/share/doc/polipo/examples/config.sample
       # and "polipo -v" for variables you can tweak and further information.
       logSyslog = false
       logFile = /var/log/polipo/polipo.log
       socksParentProxy = "127.0.0.1:1080"
       socksProxyType = socks5
       chunkHighMark = 50331648
       objectHighMark = 16384
       serverMaxSlots = 64
       serverSlots = 16
       serverSlots1 = 32
       proxyAddress = "0.0.0.0"
       proxyPort = 1087
       ```

       > * 1080: default shadowsocks socks5 port
       > * `proxyPort`: the target http port of `polipo`
       > * 1087: shadowsocksx-ng client for macOS use the default port number for http.

   * Restart the service via `/etc/init.d/polipo restart`.

3. Set the proxy for terminal.

   * `curl` is used in many command-line software in networking, it uses the `http_proxy`(lower case), `HTTPS_PROXY` and `ALL_PROXY` in environment variables.

   * `export http_proxy="127.0.0.1:1087"` for general usage.

   * Two handy shell script function for proxy.

     ```shell
     
     # ----------------------------- PROXY -----------------------------
     
     # Source from https://gist.github.com/patik/6d40ded40bf93c2f381b
     
     # configure proxy for git while on corporate network
     # From https://gist.github.com/garystafford/8196920
     function proxy_on(){
        # assumes $USERDOMAIN, $USERNAME, $USERDNSDOMAIN
        # are existing Windows system-level environment variables
     
        # assumes $PASSWORD, $PROXY_SERVER, $PROXY_PORT
        # are existing Windows current user-level environment variables (your user)
     
        # environment variables are UPPERCASE even in git bash
        # export HTTP_PROXY="http://$USERNAME:$PASSWORD@$PROXY_SERVER:$PROXY_PORT"
        export HTTP_PROXY="http://127.0.0.1:1087"
        export http_proxy=$HTTP_PROXY
        export HTTPS_PROXY=$HTTP_PROXY
        export FTP_PROXY=$HTTP_PROXY
        export SOCKS_PROXY=$HTTP_PROXY
     
        export NO_PROXY="localhost,127.0.0.1,$USERDNSDOMAIN"
     
        # Update git and npm to use the proxy
        git config --global http.proxy $HTTP_PROXY
        # git config --system http.sslcainfo /bin/curl-ca-bundle.crt
        # git config --global http.sslcainfo /bin/curl-ca-bundle.crt
        # npm config set proxy $HTTP_PROXY
        # npm config set https-proxy $HTTP_PROXY
        # npm config set strict-ssl false
        # npm config set registry "http://registry.npmjs.org/"
     
        # optional for debugging
        export GIT_CURL_VERBOSE=1
     
        # optional Self Signed SSL certs and
        # internal CA certificate in an corporate environment
        export GIT_SSL_NO_VERIFY=1
     
     
        env | grep -e _PROXY -e GIT_ | sort
        echo -e "\nProxy-related environment variables set."
     
        clear
     }
     
     # Enable proxy settings immediately
     # proxy_on
     
     # Disable proxy settings
     function proxy_off(){
        variables=( \
           "HTTP_PROXY" "http_proxy" "HTTPS_PROXY" "FTP_PROXY" "SOCKS_PROXY" \
           "NO_PROXY" "GIT_CURL_VERBOSE" "GIT_SSL_NO_VERIFY" \
        )
     
        for i in "${variables[@]}"
        do
           unset $i
        done
     
        env | grep -e _PROXY -e GIT_ | sort
        echo -e "\nProxy-related environment variables removed."
     }
     
     ```



#### tmux configuration

 Profile `~/.tmux.conf`:

```
# Allow mouse interaction
set-option -g mouse on

# Display CPU load average for the last 1,5 and 15 minutes, in the status bar
set -g status-right "#(cut -d ' ' -f -3 /proc/loadavg) %H:%M %d-%b-%y"

# make scrolling with wheels work
# https://stackoverflow.com/a/33461197
bind -n WheelUpPane if-shell -F -t = "#{mouse_any_flag}" "send-keys -M" "if -Ft= '#{pane_in_mode}' 'send-keys -M' 'select-pane -t=; copy-mode -e; send-keys -M'"
bind -n WheelDownPane select-pane -t= \; send-keys -M

# Tmux plugin manager https://github.com/tmux-plugins/tpm
# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'

# Plugin configurations
set -g @continuum-restore 'on'

# Key bindings
# prefix + I, install the above plugins and refresh existing environments.
# prefix + U, update the above plugins.
# prefix + alt + u, uninstall plugins not in the above plugin list.

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run -b '~/.tmux/plugins/tpm/tpm'

```

Reload the profile changes with command `tmux source-file ~/.tmux-config`.



#### OpenWrt

1. Setup the boot system with corresponding OpenWrt distribution, refer to [OS Setup](#OS-Setup).

2. Connect wired network cable, USB keyboard, HDMI display and power to raspberry pi.

   * Assume that wired network cable connected to main gateway(router) and work well.
   * Press `Enter` after a few seconds to launch the system to enter the welcome shell.
   * Change password via `passwd` if necessary.
   * `ifconfig -a`, show all the network interfaces.

3. Connect via Ethernet

   1. `uci set network.lan.proto=dhcp`, enable `DHCP` service.
   2. `uci set network.lan.ipaddr=192.168.1.100`, configure the default IP address.
   3. `uci set network.lan.gateway=192.168.1.1`, configure the bridge gateway(router) address.
   4. `uci commit`, commit the above changes.
   5. `service network restart`, restart the network service to apply the changes.

   Check the above changes in file `/etc/config/network`, e.g.

   ```
   config interface 'lan'
   	option type 'bridge'
   	option ifname 'eth0'
   	option netmask '255.255.255.0'
   	option ip6assign '60'
   	option proto 'dhcp'
   	option ipaddr '192.168.1.100'
     option gateway '192.168.1.1'
   ```

4. Now `ssh` should work with the above ip address.

5. Install the USB driver.

   1. EDUP driver for linux: `http://www.szedup.com/support/driver-download/ep-n8508gs-driver/`.
   2. Install `lsusb`, `opkg update && opkg install usbutils`.

6. Login to `luci` web page, e.g. `http://192.168.1.100/cgi-bin/luci/` with the `root` user.

   1. 



#### References

- Raspberry Pi Wiki Hub, http://elinux.org/RPi_Hub

- RASPI-CONFIG, [https://www.raspberrypi.org/documentation/configuration/raspi-config.md](https://www.raspberrypi.org/documentation/configuration/raspi-config.md), source: [https://github.com/RPi-Distro/raspi-config](https://github.com/RPi-Distro/raspi-config)

- RPi Advance Setup, [http://elinux.org/RPi_Advanced_Setup](http://elinux.org/RPi_Advanced_Setup)

- IP Address. [https://www.raspberrypi.org/documentation/remote-access/ip-address.md](https://www.raspberrypi.org/documentation/remote-access/ip-address.md) 
  [https://www.howtogeek.com/141157/how-to-configure-your-raspberry-pi-for-remote-shell-desktop-and-file-transfer/](https://www.howtogeek.com/141157/how-to-configure-your-raspberry-pi-for-remote-shell-desktop-and-file-transfer/)
  
- VNC. [https://www.raspberrypi.org/documentation/remote-access/vnc/README.md](https://www.raspberrypi.org/documentation/remote-access/vnc/README.md)

- Passwordless SSH Access. (ssh-keygen) [https://www.raspberrypi.org/documentation/remote-access/ssh/passwordless.md](https://www.raspberrypi.org/documentation/remote-access/ssh/passwordless.md)

- SCP. [https://www.raspberrypi.org/documentation/remote-access/ssh/scp.md](https://www.raspberrypi.org/documentation/remote-access/ssh/scp.md)

- Installing software. [https://www.raspberrypi.org/documentation/linux/software/](https://www.raspberrypi.org/documentation/linux/software/)

- Connect to WiFi network from command line.  [https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md](https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md)
  [https://unix.stackexchange.com/questions/92799/connecting-to-wifi-network-through-command-line](https://unix.stackexchange.com/questions/92799/connecting-to-wifi-network-through-command-line)
  
  https://www.raspberrypi.com/documentation/computers/configuration.html#wireless-lan-2
  
- Installing zsh.
  - Install zsh. [http://www.jamesralexander.com/blog/content/installing-zsh-on-raspberry-pi-wheezy/](http://www.jamesralexander.com/blog/content/installing-zsh-on-raspberry-pi-wheezy/)
  - Install oh-my-zsh. [https://github.com/robbyrussell/oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)
  - source ~/.zshrc .
  
- Backup. [https://www.raspberrypi.org/documentation/linux/filesystem/backup.md](https://www.raspberrypi.org/documentation/linux/filesystem/backup.md) [https://www.raspberrypi.org/magpi/back-up-raspberry-pi/](https://www.raspberrypi.org/magpi/back-up-raspberry-pi/)

- Scheduling Task with CRON. [https://www.raspberrypi.org/documentation/linux/usage/cron.md](https://www.raspberrypi.org/documentation/linux/usage/cron.md)

- How to turn a raspberry pi into an always on bittorrent box? [https://www.howtogeek.com/142044/how-to-turn-a-raspberry-pi-into-an-always-on-bittorrent-box/](https://www.howtogeek.com/142044/how-to-turn-a-raspberry-pi-into-an-always-on-bittorrent-box/)

- Installing Mono on Raspberry Pi. http://www.raspberry-sharp.org/eric-bezine/2012/10/mono-framework/installing-mono-raspberry-pi/

- Using C# Interactive Shell. http://www.raspberry-sharp.org/

- [Raspberry PI+Shadowsocks+Polipo实现科学上网](https://medium.com/@molimowang/raspberry-pi-shadowsocks-polipo%E5%AE%9E%E7%8E%B0%E7%A7%91%E5%AD%A6%E4%B8%8A%E7%BD%91-eae1b7eeb779)

- [SwitchyOmega Wiki](https://github.com/FelisCatus/SwitchyOmega/wiki/GFWList)

- [tmux-config](https://github.com/samoshkin/tmux-config)

- [OpenWrt-Project: Raspberry-Pi](https://openwrt.org/toh/raspberry_pi_foundation/raspberry_pi)

