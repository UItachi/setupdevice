# Linux

> Mainly for Debian.



## Create a normal user for normal usages ALWAYS

#### List all users

```bash
cat /etc/passwd
```

#### List all user groups

```bash
cat /etc/group
```

> Format: <group name>:<password>:<group id>:<users in the group>

List groups for the specified user

```bash
groups <username>
```

#### Create a new user group

#### Create a new user

```bash
useradd -r -g hanwei -G root
mkdir -p /home/hanwei
chown -R 
```









## References

* [Create a sudo user on debian](https://phoenixnap.com/kb/create-a-sudo-user-on-debian)
* [`user mod` command in linux](https://linuxize.com/post/usermod-command-in-linux/)

