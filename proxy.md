## V2ray Configuration

### Server

```shell
#!/usr/bin/env bash

echo "Switching to root user if necessary."
sudo -i

echo "Update the apt-get cache"
apt-get update

echo "Installing zsh"
apt-get --assume-yes install zsh

chsh -s $(which zsh)

echo "Installing git"
apt-get --assume-yes install git

echo "Installing wget"
apt-get install wget

echo "Installing oh-my-zsh"
sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

echo "Install zsh-autocompletions"
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions
echo "\n# zsh-autocompletions \nsource ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh" >> ~/.zshrc

source ~/.zshrc

# Core Operations

# Refer to 
#		1. https://www.v2ray.com/chapter_00/install.html#linuxscript
# 	2. https://yuan.ga/v2ray-complete-tutorial/
# 	3. https://toutyrater.github.io/prep/install.html
echo "Installing v2ray"
bash <(curl -L -s https://install.direct/go.sh)
# https://github.com/233boy/v2ray/wiki
# bash <(curl -s -L https://git.io/v2ray.sh)

echo "Starting v2ray"
sudo systemctl start v2ray

echo "Done!"


```

- v2ray configuration file
  `/etc/v2ray/config.json`

- v2ray software update
  `bash <(curl -L -s https://install.direct/go.sh)`, same with installing steps.

- v2ray service management

  `systemctl start|restart|stop|status v2ray`




### Client

* **macOS**

  * [homebrew-v2ray](https://github.com/v2ray/homebrew-v2ray)

    * Install

      ```shell 
      brew tap v2ray/v2ray
      brew install v2ray-core
      ```

    * Usage
      > Config file: /usr/local/etc/v2ray/config.json

      ```shell 
      brew tap v2ray/v2ray
      brew install v2ray-core
      ```

    * Update

      ```shell 
      brew services start v2ray-core # run and register to launchctl.
      brew services run v2ray-core   # run without register.
      v2ray -config /usr/local/etc/v2ray/config.json # run with interactively.
      ```

    * Uninstall

      ```shell 
      brew uninstall v2ray-core
      brew untap v2ray/v2ray
      ```

  * [V2rayU](https://github.com/yanue/v2rayu) for macOS 10.14+
  * [V2rayX](https://github.com/Cenmrev/V2RayX) for macOS 10.11+

* **iOS**
  
  > Shadowrocket
  
* **Linux**
  
  The installation steps are the same with server side, just use the different `config.json` file.
  
  > https://www.v2ray.com/chapter_00/install.html#linuxscript





---



## ~~Shadowsocks Configuration~~

```
#!/usr/bin/env bash

echo "Switching to root user if necessary."
sudo -i

echo "Update the apt-get cache"
apt-get update

echo "Installing zsh"
apt-get --assume-yes install zsh

chsh -s $(which zsh)

echo "Installing git"
apt-get --assume-yes install git

echo "Installing wget"
apt-get install wget

echo "Installing oh-my-zsh"
sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

echo "Install zsh-autocompletions"
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions
echo "\n# zsh-autocompletions \nsource ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh" >> ~/.zshrc

source ~/.zshrc

# Refer to https://teddysun.com/342.html
echo "Installing shadowsocks"
wget --no-check-certificate -O shadowsocks.sh https://raw.githubusercontent.com/teddysun/shadowsocks_install/master/shadowsocks.sh
chmod +x shadowsocks.sh
bash shadowsocks.sh 2>&1 | tee shadowsocks.log

# For digital ocean vps only.
# Refer to https://serverfault.com/a/129087
# echo "Disable the firewall with default policies explicitly."
# iptables -P INPUT ACCEPT
# iptables -P OUTPUT ACCEPT
# iptables -P FORWARD ACCEPT
# iptables -F

# To be verified!
# systemctl stop firewalld # stop the firewall service
# systemctl disable firewalld # Disable the firewall start up with system

echo "Install tmux"
apt-get --assume-yes install tmux

echo "Done!"


```


- shadowsocks configuration file `/etc/shadowsocks.json`

- shadowsocks service management

  `/etc/init.d/shadowsocks start|stop|restart|status`




## Debian Configuration

- Change passoword [for root]

  `[sudo] passwd [root]`

- [TimeZoneChanges](https://wiki.debian.org/TimeZoneChanges)
- [Configuring Locales](https://people.debian.org/~schultmc/locales.html)



## Useful Proxy Tools

|           | macOS | iOS  | Linux | Notes |
| --------- | ----- | ---- | ----- | ----- |
| [Proxifier](https://www.proxifier.com/) |  ✔  |  |  | Support to custom rules with auto-proxy. |
| [proxychains-ng](https://github.com/rofl0r/proxychains-ng) | ✔ |      | ✔ | Global proxy only, convert socks to http(s). |
| [polipo](https://www.irif.fr/~jch/software/polipo/) |       |      | ✔ | Global proxy only, convert socks to http(s). *No maintained anymore.* |
| [Shadowrocket](https://apps.apple.com/us/app/shadowrocket/id932747118) |  | ✔ | | Support ss, ser, vems, socks5, http(s), etc. |

* proxychains-ng

  * config file: `/etc/proxychains.conf`, `$(HOME)/.proxychains/proxychains.conf`, `$(sysconfdir)/proxychains.conf`.
    * For macOS: the config file also could be in `/usr/local/etc/proxychains.conf`.
  * `sudo make install-config` will generate [an initial one](https://gist.github.com/marcinwol/b8e502eede230cc33c43).

* polipo

  * config file: `/etc/polipo/config`

  * configuration:

    ```
    socksParentProxy = "127.0.0.1:1080"
    socksProxyType = socks5
    proxyPort = 8123
    
    chunkHighMark = 50331648
    objectHighMark = 16384
    serverMaxSlots = 64
    serverSlots = 16
    serverSlots1 = 32
    proxyAddress = "0.0.0.0"
    ```

    