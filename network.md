## Network CLI Utilities

A series of cli command to debug network issues.



#### ping

Test the reachability of a host on an Internet Protocol (IP) network.

* `ping <ip_addr>`
* `ping <ip_addr> -A`, output a bell (ASCII 0x07) character when no packet is received. `ping <ip_addr> -a`, include a bell (ASCII 0x07) character in the output when any packet is received.
* `ping <ip_addr> -m <ttl_value>`, with specified ttl(time to live).


&nbsp;


#### ifconfig

Configure network interface parameters.

* `ifconfig eth0 up|down`, enable/disable the eth0 interface.


&nbsp;


#### traceroute

Displaying the route (path) and measuring transit delays of packets across an Internet Protocol (IP) network.

* `traceroute <ip_addr>`

&nbsp;

#### [mtr](http://www.bitwizard.nl/mtr/)

mtr combines the functionality of the 'traceroute' and 'ping' programs in a single network diagnostic tool.

* `sudo mtr <hostname>`

&nbsp;

#### nslookup

Query Internet domain name servers interactively.

* `nslookup <domain_name>`
* `nslookup <domain_name> <name_server>`, query a given name server for a NS record of the domain.
* `nslookup -type=PTR <domain_name>`, query for a reverse lookup (PTR record) of the domain.


&nbsp;


#### dig

It performs DNS lookups and displays the answers that are returned from the name server(s) that were queried.

* `dig <domain_name>`
* `dig @<name_server> <domain_server>`, use a specific DNS server for the query.


&nbsp;


#### whois

Looks up records in the databases maintained by several Network Information Centers.

* `whois <domain_name>`


&nbsp;


#### netstat

Displays network connections for the TCP (both incoming and outgoing), routing tables, and a number of network interface (network interface controller or software-defined network interface) and network protocol statistics.

* `[sudo] lsof -i -P | grep "LISTEN"`, show all the listening processes.


&nbsp;


#### lsof

Report a list of all open files and the processes that opened them.

* `lsof -i -n -P | grep ss-local`, view the port associated with a process.
* `losf /Volumns/RemovableDisk`, view the processes which open the files under the specified path.
* `lsof path/to/file`, find the processes that have a given file open.
* `lsof -i :port`, find the process that opened a local internet port.
* `lsof -t path/to/file`, only output the process ID (PID).
* `lsof -u username`, list files opened by the given user.
* `lsof -c process_or_command_name`, list files opened by the given command or process.
* `lsof -p PID`, list files opened by a specific process, given its PID.
* `lsof +D path/to/directory`, list open files in a directory.
* `lsof -iTCP:port -sTCP:LISTEN`, find the process that is listening on a local TCP port.


&nbsp;


#### arp

Display and modify the [ARP](https://en.wikipedia.org/wiki/Address_Resolution_Protocol) table entries on the local computer. 

* `arp -a`

  > arp-scan: ARP scanning and fingerprinting tool
  >
  > arping: Utility to check whether MAC addresses are already taken on a LAN



&nbsp;


#### host

DNS lookup.

&nbsp;

#### networksetup

Configuration tool for Network System Preferences.

* `networksetup -listallnetworkservices`, list available network service providers (Ethernet, Wi-Fi, Bluetooth, etc)
* `networksetup -getinfo "Wi-Fi"`, show network settings for a particular networking device.
* `networksetup -getairportnetwork en0`, get currently connected Wi-Fi network name (Wi-Fi device usually en0 or en1).
* `networksetup -setairportnetwork en0 "Airport Network SSID" password`, connect to a particular Wi-Fi network.


&nbsp;


#### scutil

Manage system configuration parameters (using the SystemConfiguration.framework SCDynamicStore APIs).

* `scutil -r <local_address|remote-address>`, check the network reachability of the host.
* `scutil --dns`, reports the current DNS configuration.
* `scutil --proxy`, reports the current system proxy configuration.


&nbsp;

#### netcat(ncat, nc)

Reads and writes tcp or udp data.

* `nc <host> <port>`, initiating a TCP connection to the host and port.
* `nc [-z -v] <host> <startport-endport>`, port scanner.
* `nc -z localhost  1-65535`, check all the available ports on local machine.
* `nc -l <new_port>` (server) & `nc <server_host> <server_port>` (client), just communicate.
* `nc -l new_port < file` (server) & `nc server_host server_port > file` (client), send file from server to client.
  * Show progress: `pv file | nc -l new_port` (server)
* `nc -l new_port > file` (server) & `nc server_host server_port < file` (client), send file from client to server.
* `while true; do nc -l new_port < index.html; done`, build a simple web server `http://server_port:new_port` with infinite loop.

&nbsp;

#### [nmap](https://nmap.org/download.html)

[examples](https://www.cyberciti.biz/security/nmap-command-examples-tutorials/)

[nmap scripts](https://nmap.org/nsedoc/) 

&nbsp;

#### tcpdump

Display TCP/IP and other packets being transmitted or received over a network to which the computer is attached.

* `tcpdump -D`, list available network interfaces.
* `tcpdump -i eth0`, capture the traffic of a specific interface.
* `tcpdump host <domain_name>`, capture the traffic from or to a host.

&nbsp;

#### monit

With all features needed for system monitoring and error recovery. It's like having a watchdog with a toolbox on your server. See more in [home page](https://mmonit.com/monit/) and [docs](https://mmonit.com/monit/documentation/monit.html).

* Config file: `~/.monitrc`, `/etc/monitrc`, `/usr/local/etc/monitrc`, `/usr/local/etc/monitrc`, `./monitrc`
  * Default http port: `2812`
  * Default user/pass: `admin`/`monit`
* Launch with `monit`



#### netdata

Instantly diagnose slowdowns and anomalies in your infrastructure with thousands of metrics, interactive visualizations, and insightful health alarms. See more in [home page](https://www.netdata.cloud/) and [docs](https://docs.netdata.cloud/), install via

```shell
bash <(curl -Ss https://my-netdata.io/kickstart.sh) --disable-telemetry
```

* Config file: `/etc/netdata/netdata.conf`, `/opt/netdata/etc/netdata/netdata.conf`
* Dashboard page: `http://localhost:19999`
* Launch with `systemctl start metadata`