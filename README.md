## Configuration for My Devices



##### [macOS](macOS.md)

##### [Linux](linux.md)

##### [Raspberry Pi](raspberrypi.md)

##### [Home-theater-PC](Home-theater-PC.md)

##### [Proxy](proxy.md)

##### [Network](network.md)

