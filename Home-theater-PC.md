## Home Theater PC Configuration



#### Overview

* [Plex media server](#Plex-media-server)
* [Media client](#Media-client)
* [References](#References)





#### Plex media server

* Download media server installer for macOS from [the official site](https://www.plex.tv/media-server-downloads/).





#### Media client (for Plex)

* [Plex media player for Mac](https://www.plex.tv/media-server-downloads/#plex-app), a media player client from Plex, there are also other platforms for it.
* [Plex](https://itunes.apple.com/us/app/plex/id383457673?mt=8) app for iOS.
* [AcePlayer](https://itunes.apple.com/us/app/aceplayer-good-media-player/id480881925?mt=8) for iOS.
* [Plex Web App](https://app.plex.tv/desktop), it's available to access the media player in browser remotely.
* [PlexKodiConnect](https://github.com/croneter/PlexKodiConnect) via `kodi`, short for `PKC`.
  * Install kodi via `sudo apt-get install kodi`.
    * Don't launch it via `kodi` from command line, use the menu entry instead.
  * [Install PKC from PKC Repo](https://github.com/croneter/PlexKodiConnect/wiki/Install-PKC-from-PKC-Repo)
  * [Configure PKC on the First Run](https://github.com/croneter/PlexKodiConnect/wiki/Configure-PKC-on-the-First-Run)





#### Other routes

* Chromecast





#### References

- [Cast from Browser or Desktop](https://support.plex.tv/articles/201206866-cast-from-browser-or-desktop/)
- [What Is Google Chromecast?](https://www.tomsguide.com/us/what-is-google-chromecast,news-17637.html)
- [What’s the Best Plex Client For HTPC Users?](https://www.howtogeek.com/344752/whats-the-best-plex-client-for-htpc-users/)
- 
