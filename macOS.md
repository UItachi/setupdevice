## My macOS Configuration



#### Overview

* [Application](#Applications)
  * [IDE & Editors](#IDE-&-Editors)
  * [Developer Tools](#Developer-Tools)
  * [Proxy Tools](#Proxy-Tools)
  * [Media Players](#Media Players)
  * [System Tools](#System-Tools)
  * [Menubar Tools](#Menubar-Tools)
* [Command Line](#Command-Line)
* [Chrome Extensions](#Chrome-Extensions)
* [Topics](#Topics)



#### Applications

- 1 Password
- Airmail
- Duet
- Dropbox
- OneDrive
- Parallels Desktop
- PopClip
- The Unarchiver
- iZip
- OmniFocus
- OmniGraffle
- [CheatSheet](http://mediaatelier.com/CheatSheet)
- [DayOne](http://xclient.info/s/day-one.html)
- [Apple Configurator 2](https://itunes.apple.com/cn/app/apple-configurator-2/id1037126344?l=en&mt=12), a replacement of iPhone Configuration Utility.
- [Snagit](https://www.techsmith.com/snagit.html), screen capture software for mac.
- Sip Pro, an alternative color picker.
- Sketch, built for modern graphic designers, and it shows in every fibre of the app.
- [SiteSucker](https://ricks-apps.com/osx/sitesucker/), download an entire website for offline viewing. 
- [Screaming Frog SEO Spider](https://www.screamingfrog.co.uk/seo-spider/),  a small desktop program (PC or Mac) which crawls websites' links, images, CSS, script and apps from an SEO perspective.
- Maipo
- Prepo, prepare and preview App artwork and icons.
- NetSpot, discover and analyse wireless networks around you.
- [Briefs](http://toolsandtoys.net/briefs-for-mac-and-ios/s), a mockup design tool. 
- [NetNewsWire](https://ranchero.com/netnewswire/), a free and open source RSS reader. 
- [YACReader](https://www.yacreader.com/), an open source comics reader.
- [DrawnStrips Reader](https://www.seense.com/drawnstrips/), a comics reader for MacBook groundbreaking Retina display.
- [GifGrabber](https://www.gifrocket.com/), Gifrocket, create gif from video. 
- ~~TeamViewer~~
- [Time Out (dejal)](http://www.dejal.com/timeout/)
- UncrustifyX
- GoogleAppEngineLauncher
- [SteerMouse](http://plentycom.jp/en/steermouse/)
- [iExplorer](https://www.macroplant.com/iexplorer/)
- Wondershare Video Converter Ultimate, Aimersoft Video Converter Ultimate
- [Apple Remote Desktop](http://xclient.info/s/apple-remote-desktop.html)
- [bilibili mac client](https://github.com/typcn/bilibili-mac-client)
- [HelloFont](http://www.hellofont.cn/index.php)
- [iTools for Mac](http://pro.itools.cn/mac/)
- [pp助手](http://pro.25pp.com/pp_mac_ios)
- [微力同步](http://verysync.com/), replacement of Resilio-Sync. 
- [OmniDiskSweeper](https://www.omnigroup.com/more), showing you the files on your drive, in descending order by size, and letting you decide what to do with them.
- Wondershare Filmora, a video editor.
- [Telegram](https://telegram.org/), a messenger app based on the MTProto protocol. 
- [Gestimer](http://maddin.io/gestimer/), for those little reminders during the day. Drag & drop to quickly create reminders with this beautifully designed menu bar app for your Mac. 
- [Serial Box](http://xclient.info/s/serial-box.html?t=5b69e92445e78fe24c0ce0a97664cf253e74cae2)
- [KCNcrew Pack](http://xclient.info/s/kcncrew-pack.html?t=5b69e92445e78fe24c0ce0a97664cf253e74cae2)
- [Clean Text](http://xclient.info/s/clean-text.html?t=bff69292c5d68e36862ca88ae892c137c538b441), an essential tool for webmasters, graphic designers, developers and magazine editors made to reduce text cleanup and editing time.
- [wine](http://winehq.com), run windows applications in linux/macOS.
  - [winetricks](http://www.kegel.com/wine/winetricks)
- [zenity](https://help.gnome.org/users/zenity/), a GNOME port of dialog.
- [Reggy](http://reggyapp.com/), a small OS X cocoa/objective-c application to very quickly visualize what a given regular expression will match given a test string. 
- [Tor browser](https://www.torproject.org/download/download-easy.html)
- [AssetCatalogTinkerer](https://github.com/insidegui/AssetCatalogTinkerer), an app that lets you open .car files and browse/extract their images.
- [EasyScreenOCR](https://apps.apple.com/cn/app/screen-ocr/id1487414909?l=en&mt=12)
- [OBS Studio](https://obsproject.com/), free and open source software for video recording and live streaming.
- [Xee](https://theunarchiver.com/xee), a streamlined and convenient image viewer and browser. [download](https://xclient.info/s/xee3.html).
- [Phoenix Slides](https://blyt.net/phxslides/), a fast full-screen slideshow program/image browser, for flipping through folders or disks full of images.



##### IDE & Editors

* Visual Studio Code
* Sublime Text
* PyCharm
* ~~Evernote, Alternote~~
* Typora
* [Texts](http://www.texts.io/)
* [Textmate](https://macromates.com/)
* Outline, like Microsoft OneNote
* Xamarin Studio
* [MacTeX](https://www.tug.org/mactex/index.html) for latex.
* [Spyder](https://www.spyder-ide.org/), a free and open source scientific environment written in Python, for Python, and designed by and for scientists, engineers and data analysts.
* [MarkText](https://github.com/marktext/marktext), a simple and elegant markdown editor.
* [Hex Friend](https://hexfiend.com/), a fast and clever open source hex editor for macOS.
* [Paintbrush](https://paintbrush.sourceforge.io/), an original simple paint program for macOS. 
* [neovide](https://neovide.dev/), a simple, no-nonsense, cross-platform graphical user interface for [Neovim](https://github.com/neovim/neovim).



##### Developer Tools

* [SnippetsLab](https://www.renfei.org/snippets-lab/), an easy-to-use code snippets manager.

* Cornerstone, a gui client for svn.

* PaintCode

* iTerm2

* DiffMerge

* Source Tree

* [Source Git](https://sourcegit-scm.github.io/), an open-source git client.

* Git Tower

* Reveal

* Font Book

* Paw

* [Postman](https://www.getpostman.com/downloads/)

* [CocoaPods](https://cocoapods.org/app)

* [LogTail](http://www.logtailapp.com/), a modern Document-based Cocoa App for macOS (10.12+) to view and monitor log files on your local system and on remote servers over SSH.

* [Cronette](http://xclient.info/s/cronette.html?t=bff69292c5d68e36862ca88ae892c137c538b441), a gui client like crontab.

* [kite](https://kite.com/), a plugin for your IDE that uses machine learning to give you 
  useful code completions for Python. 
  
* [Accessorizer](http://www.kevincallahan.org/software/accessorizer.html), an objective-c language code generator. 

* [Oh My Star](http://xclient.info/s/oh-my-star.html), Github Stared Repo management tool.

* [VirtualBox](https://www.virtualbox.org/), a powerful x86 and AMD64/Intel64 virtualization product.

* [nox](https://www.yeshen.com/), android emulator.

* [Mumu player](https://goongloo.com/download/mumu-player/), android emulator.

* [Android Studio Emulator](https://developer.android.com/studio/run/managing-avds.html), android emulator (AVD).

* [Transmit](https://www.panic.com/transmit/), a file transfer and manager client for remote servers.

* [Kaleidoscope](https://kaleidoscope.app/), a powerful text/image/folder comparison app.

* [DocFetcher](http://docfetcher.sourceforge.net/en/index.html), search contents in your local files.

* [Knuff](https://github.com/KnuffApp/Knuff), designed for debugging iOS APNs.

* [DevUtils-app](https://github.com/DevUtilsApp/DevUtils-app), an offline toolbox for developers.

* [Sublime-Pretty-Shell](https://github.com/aerobounce/Sublime-Pretty-Shell), shell script formate / syntax checker.

* [Burp Suite](https://portswigger.net/burp), an application security testing software.

* [orbstack](https://orbstack.dev/), lightweight docker app.

* [Zed](https://zed.dev/), a high-performance, multiplayer code editor from the creators of Atom and Tree-sitter.

* [Warp](https://www.warp.dev/), a terminal reimagined with AI and collaborative tools for better productivity.

* [RuntimeBrowser](https://github.com/nst/RuntimeBrowser/), a class browser for Objective-C runtime on macOS.



##### Proxy Tools

* [V2rayU](https://github.com/yanue/v2rayu/)
* Proxifier
* ~~[polipo](https://www.irif.fr/~jch/software/polipo/#:~:text=Polipo%20is%20a%20small%20and,used%20by%20a%20larger%20group.)~~
* [macproxy](http://www.tidalpool.ca/macproxy/)
* VRouter
* flora-kit
* SSHMode, a LAN proxy tool like ssh port forwarding.
* [Wireshark](https://www.wireshark.org/) 
* [CocoaPacketAnaylzer](https://www.tastycocoabytes.com/), a native macOS implementation of a network protocol analyzer and packet sniffer.
* [SquidMan](http://squidman.net/squidman/index.html), operate as a "personal" proxy server.
* [mitmproxy](https://mitmproxy.org/), an interactive console program that allows traffic flows to be intercepted, inspected,modified and replayed. 



##### Media Players

* [mpv](https://mpv.io/), a video player based on MPlayer/mplayer2.
* [IINA](https://iina.io),  a modern macOS player.
* [Spotify](http://download.spotify.com/Spotify.dmg)
* AirServer, mirror iPhone screen to mac.
* [PlayCover](https://playcover.io/), run iOS apps and games on Apple Silicon Mac.



##### Downloader & Converter

* Thunder, uTorrent
* Downie
* Softorino YouTube Converter
* [aMule](http://www.amule.org/)
* AriaNg Native
* [Photon](https://github.com/alanzhangzm/Photon), a lightweight multi-threaded downloader based on aria2.
* [NeatDownloadManager](https://www.neatdownloadmanager.com/index.php/en/) 



##### System Tools

* [AppCleaner](http://freemacsoft.net/appcleaner/)
* [Cleaner One](https://apps.apple.com/cn/app/cleaner-one-lite-disk-clean/id1473079126?l=en&mt=12) 
* [Little Snitch](http://xclient.info/s/little-snitch.html)
* [Stats](https://github.com/exelban/stats), macOS system monitor in your menu bar.
* [coconutBattery](http://www.coconut-flavour.com/coconutbattery/), it shows you live information about the battery in your Mac and iOS device.
* Battery Health, Monitor Battery Stats and Usage.
* [PreferenceCleaner](http://www.echomist.co.uk/software/PreferenceCleaner.php)
* [Karabiner]((https://pqrs.org/osx/karabiner/)), a powerful and stable keyboard customizer for OS X.
* [KeyKey Typing Tutor](https://xclient.info/s/keykey.html), keyboard typer.
* [slate](https://github.com/jigish/slate), a window management application (replacement for Divvy/SizeUp/ShiftIt).
* [uBar](https://brawersoftware.com/products/ubar), the Dock replacement for the Mac.
* [Contexts](https://contexts.co/), switch between application windows effortlessly. 
* [LaunchControl](http://www.soma-zone.com/LaunchControl/), a fully-featured launchd GUI allowing you to create, manage and debug system- and user services on your Mac.
* [Lingon X](https://www.peterborgapps.com/lingon/), Lingon can start an app, a script or run a command automatically whenever you want it to. 
* [Manico](https://itunes.apple.com/cn/app/manico/id724472954?l=en&mt=12)
* [Thor](https://itunes.apple.com/cn/app/thor/id1120999687?l=en&mt=12), custom application switcher with hotkeys. 
* Tuxera NTFS
* [TripMode](https://www.tripmode.ch/)
* [OSXFuse](https://osxfuse.github.io/), [sshfs](https://github.com/osxfuse/sshfs), [bindfs](https://bindfs.org/).
* [SwitchHosts!](https://github.com/oldj/SwitchHosts), switch hosts quickly!
  * [googlehosts](https://github.com/googlehosts/hosts) 
* [HostsToolforMac](https://github.com/ZzzM/HostsToolforMac) 
* [Hosts.prefpane](https://github.com/specialunderwear/Hosts.prefpane/), a Cocoa GUI for `/etc/hosts`.
* [Vimac](https://github.com/dexterleng/vimac), Vimium for macOS.
* [nerd font](https://www.nerdfonts.com/), Iconic font aggregator, collection, and patcher.




##### Menubar Tools

- Bartender
- [Hidden Bar](https://github.com/dwarvesf/hidden), hide menu bar items like `Bartender` but open-source.
- Alfred
- [Raycast](https://www.raycast.com/), an AI powered spotlight app.
- [AppTrap](http://onnati.net/apptrap/)
- Dash
- [Moom](https://manytricks.com/moom/)
- HyperDock Helper, an docker helper manager tool.
- HyperSwitch
- [Flycut](https://github.com/termit/flycut)
- CopyClip, clipboard history manager.
- [teleportd](http://abyssoft.com/software/teleport/), use a single mouse and keyboard to control several Macs.
- cDock
- TotalFinder
- [iRightMouse](https://apps.apple.com/cn/app/%E8%B6%85%E7%BA%A7%E5%8F%B3%E9%94%AE-irightmouse/id1497428978?l=en&mt=12)
- PingMenu, should NOT be replaced with iStat Menus.
- [iStat Menus](https://bjango.com/mac/istatmenus/), an advanced Mac system monitor for your menubar.
- [Today Scripts](https://github.com/megabitsenmzq/Today-Scripts), a widget for running scripts in the Today View.
- EuDic
- [TinkerTool](http://www.bresink.com/osx/TinkerTool.html)
- Memory Clean
- [arSync](http://arrsync.sourceforge.net/)
- [Sync Folders [Pro]](http://www.greenworldsoft.com/sync-folders-pro-help.php)
- [Tickets](http://www.yingdev.com/projects/tickeys)
- [web2app](http://xclient.info/s/web2app.html)
- [MenubarX](https://apps.apple.com/cn/app/menubarx/id1575588022), a webbrowser in menubar.
- [unite](https://www.bzgapps.com/unite), turn any website into a deeply customizable app.
- [nativefier](https://github.com/nativefier/nativefier), make any web page a desktop application.
- [Hazel](http://xclient.info/s/hazel.html)
- [WidgetManager](https://downtownsoftwarehouse.com/software/WidgetManager/)
- [Jitouch](http://xclient.info/s/jitouch.html)
- [launchrocket](https://github.com/jimbojsb/launchrocket), a Mac PrefPane to manage all your Homebrew-installed services.
- [TextExpander](http://xclient.info/s/textexpander.html)
- [TextSoup](https://xclient.info/s/textsoap.html)
- Monosnap, screen snap tool.
- New File Menu, create kinds of files in right context menu.
- [fliqlo](http://fliqlo.com/), clock screensaver.
- [PrefsEditor](http://apps.tempel.org/PrefsEditor/), a GUI for the 'defaults' command. 
- [RB App Checker Lite](https://brockerhoff.net/RB/AppCheckerLite/), an easy way to check application code signatures and receipts.
- [FastScripts](https://red-sweater.com/fastscripts/), a powerful script management utility.
- [RunCat](https://itunes.apple.com/cn/app/runcat/id1429033973?l=en&mt=12), lives in menu bar with CPU usage.
- [Startupizer](http://gentlebytes.com/startupizer/), an advanced yet simple to use login items handler.
- [OpenWebMonitor](http://fatecore.com/p/OpenWebMonitor/), monitor all kinds of webpages you want as a local service.
- [Do Not Disturb](https://digitasecurity.com/donotdisturb/), alerts you to Evil Maid attacks and other unauthorized access to your unattended MacBook. [iOS app](https://itunes.apple.com/cn/app/do-not-disturb-companion/id1345055731?mt=8)
- [KnockKnock](https://objective-see.com/products/knockknock.html), see what's persistently installed on your Mac.
- [RansomWhere](https://objective-see.com/products/ransomwhere.html), continually monitoring the file-system for the creation of encrypted files by suspicious processes.
- [OverSight](https://objective-see.com/products/oversight.html), monitors a mac's mic and webcam, alerting the user when the internal mic is activated, or whenever a process accesses the webcam.
- [Effortless](https://itunes.apple.com/cn/app/effortless/id1368722917?l=en&mt=12), a menu bar app to do list and task manager that helps you stay focused on one thing at a time. 
- [StartUp](https://itunes.apple.com/cn/app/startup/id1452212620?l=en&mt=12), a simple free application to create basic LaunchDeamon or LaunchAgent scheduler PLIST files. 
- [Crontab Creator](https://itunes.apple.com/cn/app/crontab-creator/id1438725196?l=en&mt=12)
- [Mos](https://mos.caldis.me/), a lightweight tool used to smooth scrolling and set scroll direction independently for your mouse. 
- [Superbar](https://www.superbar.app/), customize what’s possible from your Mac menubar. 
- [objection](https://github.com/sensepost/objection), runtime mobile exploration. 
- [WeChatTweak-macOS](https://github.com/Sunnyyoung/WeChatTweak-macOS), a dynamic library tweak for WeChat macOS. 
- [WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac), an extension for WeChat.
- [lsyncd](https://axkibe.github.io/lsyncd/)
- [ShowyEdge](https://showyedge.pqrs.org/), a visible indicator of the current input source for macOS.
- [Maccy](https://maccy.app/), clipboard manager for macOS which does one job - keep your copy history at hand.
- [iShot](https://www.better365.cn/ishot.html), screen shot tool developed by Chinese developer.




#### Command Line

- [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)
- [zsh-syntax-highlighting](https://github.com/zsh-users/zsh-syntax-highlighting)
- [zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions)
- [antigen](https://github.com/zsh-users/antigen), plugin manager for zsh.
- [brew](http://brew.sh/)
- [tldr](https://github.com/tldr-pages/tldr)
- [cheat](https://github.com/chrisallenlane/cheat)
- wget
- [you-get](https://github.com/soimort/you-get)
- [youtube-dl](https://github.com/rg3/youtube-dl)
- [ffmpeg](https://www.ffmpeg.org/)
- [you-get](https://you-get.org/)
- [fisherman](https://fisherman.github.io/), the fish-shell plugin manager. 
- bash_completion
- [tig](https://github.com/jonas/tig), text-mode interface for git.
- [ranger](https://github.com/ranger/ranger), a vim-inspired file manager for console.
- pip
- httpie, http-prompt
- tree
- [sshrc](https://github.com/Russell91/sshrc)
- tmux, a clean, modern, BSD-licensed terminal multiplexer, similar to GNU screen.
- [tpm](https://github.com/tmux-plugins/tpm), a tmux plugin manager. 
- cocoapods
- lua, luarocks
- telnet/netcat(nc)
- nmap/Zenmap
- trash, rmtrash
- [fd](https://github.com/sharkdp/fd), a simple, fast and user-friendly alternative to `find`. 
- [nb](https://github.com/xwmx/nb), a notebook management system base on git.
- ack
- [ag](https://github.com/ggreer/the_silver_searcher)(the_silver_searcher), a code-searching tool similar to ack. 
- [ripgrep](https://github.com/BurntSushi/ripgrep), recursively searches directories for a regex pattern. 
- hhighlighter
- nomad-cli, shenzhen
- [Homebrew Cask](https://github.com/caskroom/homebrew-cask)
- [git-up for git](https://aanandprasad.com/git-up/)
- [git-imerge for git](https://github.com/mhagger/git-imerge)
- [git-sweep for git](https://github.com/arc90/git-sweep)
- [git-spindle](https://seveas.github.io/git-spindle/)
- [diff-so-fancy](https://github.com/so-fancy/diff-so-fancy)
- [hub](https://hub.github.com/), a command-line wrapper for git that makes you better at GitHub. 
- [thefuck](https://github.com/nvbn/thefuck)
- [chisel](https://github.com/facebook/chisel), a collection of LLDB commands to assist debugging iOS apps. 
- [infer](http://fbinfer.com/docs/getting-started.html), a static analysis tool for Java, Objective-C and C. 
- fir-cli, fir.im command-line interface.
- [http-prompt](https://github.com/eliangcs/http-prompt)
- [prompt-toolkit (for python)](https://github.com/jonathanslenders/python-prompt-toolkit)
- [pdb++](https://github.com/pdbpp/pdbpp), a drop-in replacement for pdb. 
- [chisel](https://github.com/facebook/chisel)
- [gitsh](https://github.com/thoughtbot/gitsh)
- [liftoff](https://github.com/thoughtbot/liftoff)
- unix2dos, dos2unix
- [cocoapods-deintegrate](https://github.com/CocoaPods/cocoapods-deintegrate)
- [cocoapods-dependencies](https://github.com/segiddins/cocoapods-dependencies)
- [cocoapods-open](https://github.com/leshill/open_pod_bay)
- [cocoapods-packager](https://github.com/CocoaPods/cocoapods-packager)
- [dataconverters](http://okfnlabs.org/dataconverters/), Unified python library and command line interface to convert data from one format to another (especially tabular data). 
- [duc](http://duc.zevv.nl/#), a collection of tools for inspecting and visualizing disk usage. 
- [ncdu](https://dev.yorhel.nl/ncdu), a disk usage analyzer with an ncurses interface. 
- [fdupes](https://github.com/adrianlopezroche/fdupes), finds duplicate files in a given set of directories.
- [yank](https://github.com/mptre/yank), copy terminal output to clipboard. 
- [lolcat](https://github.com/busyloop/lolcat), rainbows and unicorns! 
- [fzf](https://github.com/junegunn/fzf), a command-line fuzzy finder. 
- [peco](https://github.com/peco/peco), a simplistic interactive filtering tool like `fzf`. 
- [vifm](https://vifm.info/), ncurses based file manager with vi like keybindings. 
- [bypy](https://github.com/houtianze/bypy), a python client for Baidu Yun (Personal Cloud Storage). 
- [BaiduPCS-Go](https://github.com/iikira/BaiduPCS-Go), a command line client written by go for Baidu Yun. 
- [pet](https://github.com/knqyf263/pet), a simple command-line snippet manager. 
- [just](https://github.com/casey/just), a command runner.
- [watchexec](https://github.com/watchexec/watchexec), executes commands in response to file modifications.
- [lnav](http://lnav.org/), an advanced log file viewer for the small-scale. 
- [beets](https://beets.readthedocs.io/en/v1.4.7/index.html), the music geek’s media organizer. 
- [fx](https://github.com/antonmedv/fx), a terminal JSON viewer. 
- [jq](https://stedolan.github.io/jq/), a lightweight and flexible command-line JSON processor. 
- [jid](https://github.com/simeji/jid), json incremental digger.
- [dsq](https://github.com/multiprocessio/dsq), a command line tool for running SQL queries against JSON, CSV, Excel, Parquet, and more.
- [xsv](https://github.com/BurntSushi/xsv), A fast CSV command line toolkit written in Rust.
- [yq](https://github.com/kislyuk/yq), a cli YAML and XML processor.
- [yamllint](https://github.com/adrienverge/yamllint), a linter for YAML files.
- [bpython](https://bpython-interpreter.org/), a fancy curses interface to the Python interactive interpreter. 
- [bat](https://github.com/sharkdp/bat), a cat clone with wings. 
- mono
- osquery
- icu
- hr
- aria2
- [macports](https://guide.macports.org/chunked/installing.macports.html), an easy to use system for compiling, installing, and managing open source software. 
- autoenv
- autojump
- (awesome-shell project list in github)
- nokogiri, an HTML, XML, SAX, and Reader parser with XPath and CSS selector support.
- [gym](https://github.com/fastlane/gym), builds and packages iOS apps for you. is a replacement for [shenzhen](https://github.com/nomad/shenzhen).
- [xcpretty](https://github.com/supermarin/xcpretty), Flexible and fast xcodebuild formatter. 
- [icdiff](https://github.com/jeffkaufman/icdiff)
- [DayOne CLI Man](http://dayoneapp.com/tools/cli-man/)
- [gitstats](http://gitstats.sourceforge.net/)
- [cmus](https://cmus.github.io/), a small, fast and powerful console music player for Unix-like operating systems.
- [musicbox](https://github.com/darknessomi/musicbox/), a cli version of netease music.
- [mpg123](http://mpg123.org/), fast console MPEG Audio Player and decoder library.
- lunch
- [tsocks](https://sourceforge.net/projects/tsocks/?source=navbar),provides transparent network access through a SOCKS version 4 or 5 proxy (usually on a firewall). [tsocks](https://sourceforge.net/projects/tsocks/?source=navbar) intercepts the calls applications make to establish TCP connections and transparently proxies them as necessary. 
- [websocat](https://github.com/vi/websocat), a command-line client for WebSockets, like netcat (or curl) for ws:// with advanced socat-like functions.
- [fswatch](https://github.com/emcrisostomo/fswatch), a cross-platform file change monitor with multiple backends.
- [qqbot](https://github.com/pandolia/qqbot), a conversation robot base on Tencent's SmartQQ.
- [ios-webkit-debug-proxy](https://github.com/google/ios-webkit-debug-proxy), a DevTools proxy (Chrome Remote Debugging Protocol) for *iOS* devices (Safari Remote Web Inspector).
- nginx
- [ios-deploy](https://github.com/phonegap/ios-deploy), Install and debug iPhone apps from the command line, without using Xcode. 
- [iStats](http://chris911.github.io/iStats/), a command-line tool that allows you to easily grab the CPU temperature, fan speeds and battery information on OS X. 
- [Torrench](https://kryptxy.github.io/torrench/), a command-line program to search and download torrents from torrent-hosting sites. 
- [pngquant](https://pngquant.org/), a command-line utility and a library for lossy compression of PNG images. 
- [cmakelint](https://github.com/richq/cmake-lint), check for coding style issues in CMake files. 
- [autopep8](https://pypi.python.org/pypi/autopep8), automatically formats Python code to conform to the [PEP 8](https://www.python.org/dev/peps/pep-0008/) style guide.
- [Metalink checker](https://github.com/metalink-dev/checker), a command line downloader written in Python that supports multi-source downloads and chunk checksums, as well as checking mirrors. It can also be used as a Python library. 
- [asciinema](https://asciinema.org/), a free and open source solution for recording terminal sessions and sharing them on the web. 
- ncftp, a ftp client.
- [bmon](https://github.com/tgraf/bmon), an interface bandwidth monitor. 
- [tcptrack](https://linux.die.net/man/1/tcptrack), monitor status of TCP connections on a network interface. 
- [visidata](https://github.com/saulpw/visidata), a terminal spreadsheet multitool for discovering and arranging data. 
- [rainbowstream](https://github.com/orakaro/rainbowstream), A smart and nice Twitter client on terminal written in Python. 
- [tesseract](https://github.com/tesseract-ocr/tesseract), an open source OCR engine. 
- [glogg](http://glogg.bonnefon.org/), a multi-platform GUI application to browse and search through long or complex log files. 
- [cloc](https://github.com/AlDanial/cloc), counts blank lines, comment lines, and physical lines of source code in many programming languages. 
- [boxes](http://boxes.thomasjensen.com/download.html), a command line ASCII boxes unlimited! 
- [prettyping](https://denilsonsa.github.com/prettyping/), a wrapper around the standard ping tool, make the output prettier, colorful, compact and easier to read. 
- [gping](https://github.com/orf/gping), ping with a graph.
- [spark](http://zachholman.com/spark), horizontal histogram in the shell.  
- [ArchiveBox](https://github.com/pirate/ArchiveBox)
- [slurm](http://www.github.com/mattthias/slurm/), a network load monitor. 
- [git-ftp](https://github.com/git-ftp/git-ftp), uses Git to upload only changed files to FTP servers. 
- [watch](https://linuxize.com/post/linux-watch-command/), run any arbitrary commands at regular intervals and displays the output of the command on the terminal window. 
- pstree, show ps output as a tree.
- [csvkit](https://csvkit.readthedocs.io/en/latest/), a suite of command-line tools for converting to and working with CSV, the king of tabular file formats. 
- [hub](https://hub.github.com/), an extension to command-line git. 
- [bitbucket-cli](https://bitbucket.org/zhemao/bitbucket-cli/), a command-line helper for BitBucket. 
- [spellcheck](https://www.shellcheck.net/), finds bugs in your shell scripts. 
- [cookiecutter](https://github.com/cookiecutter/cookiecutter), a command-line utility that creates projects from cookiecutters (project templates). 
- [zbar](http://zbar.sourceforge.net/), read bar codes from various sources, used with `zbarimg`, available with brew.
- [qrencode](https://fukuchi.org/works/qrencode/index.html.en), encode input data in a QR Code and save as a PNG or EPS image, available with brew.
- [mediainfo](https://mediaarea.net/en/MediaInfo), a convenient unified display of the most relevant technical and tag data for video and audio files.
- [figlet](http://www.figlet.org/), a program for making large letters out of ordinary text.
- [cmatrix](https://github.com/abishekvashok/cmatrix),  show screensaver from The Matrix website.
- [direnv](https://direnv.net/), load and unload environment variables depending on the current directory.
- [qemu](https://www.qemu.org/),  a generic and open source machine emulator and virtualizer.
- [UTM](https://github.com/utmapp/UTM), virtual machines for iOS and macOS.
- [khal](https://github.com/pimutils/khal), a CLI calendar application.
- [cadaver](http://www.webdav.org/cadaver/),  a command-line [WebDAV](http://www.webdav.org/) client for Unix.
- [idb](https://fbidb.io/), iOS Development Bridge, a command line interface for automating iOS Simulators and Devices.
- [translate-shell](https://github.com/soimort/translate-shell), command-line translator using Google Translate, Bing Translator, Yandex.Translate, etc.
- [mycli](https://github.com/dbcli/mycli), a terminal client for MySQL with autocompletion and syntax highlighting.
- [nudoku](https://jubalh.github.io/nudoku/), sudoku game for your terminal.
- [ImageSnap](https://github.com/rharder/imagesnap), a public domain command-line tool that lets you capture still images from an iSight or other video source.
- [shfmt](https://github.com/mvdan/sh), a shell parser, formatter, and interpreter with bash support; includes shfmt.
- [croc](https://github.com/schollz/croc), easily and securely send things from one computer to another.
- [class-dump](http://stevenygard.com/projects/class-dump/), examine the Objective-C runtime information stored in Mach-O files.
- [speedtest-cli](https://github.com/sivel/speedtest-cli), test internet bandwidth using speedtest.net.
- [mdbook](https://rust-lang.github.io/mdBook/index.html), create books with Markdown.
- [mermaid-cli](https://github.com/mermaid-js/mermaid-cli), a cli for [mermaid](https://mermaid.js.org/) which takes a mermaid definition file as input and generates an svg/png/pdf file as output.
- [d2](https://github.com/terrastruct/d2), a modern diagram scripting language that turns text to diagrams.
- [libimobiledevice](https://libimobiledevice.org), a cross-platform FOSS library written in C to communicate with *iOS* devices natively.
- [markitdown](https://github.com/microsoft/markitdown), a python tool for converting files and office documents to Markdown.
- [pb](https://github.com/xwmx/pb), a tiny wrapper combining pbcopy & pbpaste in a single command.
- [pngpaste](https://github.com/jcsalterego/pngpaste), paste PNG into files, much like pbpaste does for text.



#### Chrome Extensions

- Vimium/cVim
- OneTab
- Xmarks Bookmark Sync
- 1 Password
- crxMouse Chrome Gestures
- Evernote Web Clipper
- Clearly
- Save to Pocket
- Proxy SwitchyOmega
- [octotree](https://github.com/buunguyen/octotree)
- Insight.io for Github, improve GitHub code browsing experience by applying code intelligence.
- Stylish
- Chromo Download Manager
- Personal Blocklist
- Advertising Terminator
- Clutter Free - Prevent duplicate tabs
- cookies.txt, exports your cookies super fast in a format compatible with wget, curl, aria2, and many more.
- Copy All URLs, copy tabs URL to clipboard (formats : text, HTML, JSON or custom). Paste to open multiple URL at one go.
- User-Agent Switcher for Chrome
- Requestly, redirect url, modify headers etc.

- Adblock Plus
- Adkill and Media Download
- Google Translator
- Bluk Download Images(ZIG)
- Fatkun Batch Download Image
- MaDe
- Pretty New Tab
- Quick Note
- Textdown
- The QR Code Extension
- Video Downloader professional
- github-awesome-autocomplete
- WrapAPI
- [Tamper](https://dutzi.github.io/tamper/), a mitmproxy based devtools extension that lets you edit remote files locally and serve them directly to Chrome. 
- delugesiphon
- Toby (mini): manage your tabs
- SuperSorter / Bookmark Sentry (scanner), a bookmark scanner that checks for duplicate and bad links.



#### Topics

##### Vim

- vimrc
- neovim
- [spacevim](https://spacevim.org)
- YouCompleteMe
- solarized
- syntastic
- vim-colors-solarized
- [vim-anywhere](https://github.com/cknadler/vim-anywhere)



##### Ruby

* [Install rvm](https://stackoverflow.com/a/38194139/1677041).

  ```shell
  curl -sSL https://get.rvm.io | bash -s stable
  ```

* Install ruby. 

  ```shell
  rvm install ruby-2.4.1
  ```

* [Install Homebrew](https://brew.sh/).



##### Rust

* Install [rust](https://www.rust-lang.org/tools/install) and [cargo](https://doc.rust-lang.org/cargo/) via [rustup](https://github.com/rust-lang/rustup).

  ```shell
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
  ```

  * Export cargo executable path to shell profile: `export $PATH="$PATH:$HOME/.cargo/bin"`
  * Update via `rustup update`.



##### Tor Browser

1. Download the [installer](https://www.torproject.org/projects/torbrowser.html.en), verify the signature and install it.
   * [Tor mirrors](https://www.torproject.org/getinvolved/mirrors.html.en).
2. Launch the vpn/shadowsocks if available.
3. Launch the Tor browser.
   * If it failed to connect to Tor, try the [BridgeDB](https://bridges.torproject.org/) (pluggable transport), [here](https://www.torproject.org/docs/bridges.html.en) is the doc.
   * [BridgeDB options](https://bridges.torproject.org/options)
4. [FAQ](https://www.torproject.org/docs/faq.html.en).

